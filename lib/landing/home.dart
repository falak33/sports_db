import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:sportsdb/model/country.dart';
import 'package:dio/dio.dart';
import 'package:sportsdb/all_sports/league.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  var defaultCountries;
  //Future<Country> allCountries;
  Future<List<Country>> allCountries;
  //List<Country> allCountries = List<Country>();
  List<Country> countries = [];
  //List<Country> filteredCountries = [];
  final String url = "https://www.thesportsdb.com/api/v1/json/1/all_countries.php";
  //var client = http.Client();
  //bool loading = true;
  var dio = Dio();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<List<Country>> fetchCountry() async {
    var response = await http.get(
      url
    );
    //print(response.statusCode);
    //print(response.data);
    if(response.statusCode == 200){
      //print(json.decode(response.body)['countries']);
      var jsonData = json.decode(response.body)['countries'];
      for(var c in jsonData){
        Country country = Country(c['name_en']);
        countries.add(country);
      }
      //print(countries.length);
      return countries;
      //return Country.fromJson(json.decode(response.body)['countries']);
      //return List<Map<String, dynamic>>.from(json.decode(response.data)['countries']);
    } else {
      throw Exception('Failed to load');
    }
  }

  @override
  void initState(){
    super.initState();
    defaultCountries = ['India', 'United States', 'Australia', 'China', 'Argentina', 'Canada'];
    allCountries = fetchCountry();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).primaryColor,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100.0),
        child: AppBar(
          elevation: 0.0,
          title: Container(
            padding: EdgeInsets.only(top:20.0),
            child: Center(
              child: Text(
                'The Sports DB',
                style: Theme.of(context).textTheme.headline2,
              ),
            ),
          )
        ),
      ),
      body: Container(
        child: Container(
            padding: EdgeInsets.all(18.0),
            child: FutureBuilder(
              //itemCount: countries.length,
              future: allCountries,
              builder: (context, AsyncSnapshot snapshot){
                if(snapshot.hasData){
                  return ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index){
                      if(defaultCountries.contains(snapshot.data[index].name_en)){
                        return FlatButton(
                          onPressed: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => League(country: snapshot.data[index].name_en)),
                            );
                          },
                          child: Card(
                            child: Container(
                                padding: EdgeInsets.only(top: 8.0, left: 12.0, bottom: 8.0, right: 12.0),
                                child: ListTile(
                                  leading: Text(
                                    snapshot.data[index].name_en,
                                    style: Theme.of(context).textTheme.bodyText1,
                                  ),
                                  trailing: Icon(
                                    Icons.arrow_forward,
                                    color: Colors.black,
                                  ),
                                )
                            ),
                          ),
                        );
                      }
                      return Container();
                    },
                  );

                } else if(snapshot.hasError){
                  return Text("${snapshot.error}");
                }
                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            )
        ),
      ),
    );
  }
}